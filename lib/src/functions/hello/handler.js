import { formatJSONResponse } from '@libs/apiGateway';
import { middyfy } from '@libs/lambda';
import { convertPoundToKilogram } from "@libs/converter";
const hello = async (event) => {
    try {
        const valueInKilogram = convertPoundToKilogram(event.body.value);
        return formatJSONResponse({
            valueInPound: event.body.value,
            valueInKilogram: valueInKilogram,
        });
    }
    catch (e) {
        console.error(e);
    }
    return formatJSONResponse({
        message: "Fail to convert the value in kilogram.",
    }, 400);
};
export const main = middyfy(hello);
//# sourceMappingURL=handler.js.map