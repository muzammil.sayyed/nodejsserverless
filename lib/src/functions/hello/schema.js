export default {
    type: "object",
    properties: {
        value: { type: 'number' }
    },
    required: ['value']
};
//# sourceMappingURL=schema.js.map